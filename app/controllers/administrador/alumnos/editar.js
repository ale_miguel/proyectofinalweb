import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		guardar(){
			this.get('model.student').save().then(()=>{
				alert("La información del alumno ha sido actualizada.")
				this.transitionToRoute('administrador.alumnos.perfil', this.get('model.student').id);
			})
			
		},
		cancelar(){
			if(confirm("¿Seguro de que quieres salir sin guardar?")){
				this.get('model.student').rollbackAttributes();
				this.transitionToRoute('administrador.alumnos.index')
			}
		}
	}
});
