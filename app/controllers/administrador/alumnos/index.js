import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		perfil(student){
			this.transitionToRoute('administrador.alumnos.perfil', student.id)
		},
		editar(student){
			this.transitionToRoute('administrador.alumnos.editar', student.id)
		},
		regresar(){
			this.transitionToRoute('administrador.inicio')
		},
		filterByName(param){

			if(param != ''){
				return this.store.query('student', {name:param})
			}
			else{
				return this.store.findAll('student')
			}
		}
	}
});
