import Controller from '@ember/controller';

export default Controller.extend({
	verCalif:false,
	init(){
		this.set('verCalif', false)
	},
	actions:{
		editar(){
			this.transitionToRoute('administrador.alumnos.editar', this.get('model.student').id);
		},
		regresar(){
			this.transitionToRoute('administrador.alumnos');
		},
		verCalificaciones(){
			if(this.get('verCalif')){
				this.set('verCalif', false);
			}
			else{
				this.set('verCalif', true)
			}
		}
	}
});
