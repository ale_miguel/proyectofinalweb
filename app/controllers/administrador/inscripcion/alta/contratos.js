import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		guardar(){
			this.get('model').save().then((contrato)=>{
				this.transitionToRoute('administrador.inscripcion.alta.alumnos', contrato.id);
			})
			
		},
		cancelar(){
			if(confirm("¿Estás seguro de que deseas cancelar?")){
				this.transitionToRoute('administrador.inicio');
			}
			
		}
	}
});
