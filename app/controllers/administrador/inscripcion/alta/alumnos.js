import Controller from '@ember/controller';

export default Controller.extend({
  nuevo: false,
  nuevoStudent: null,
  isEditing: false,
  studentEditar: null,

  actions: {
    guardar() {
      this.transitionToRoute('administrador.inicio')
    },
    guardarNino(nino) {
      this.set('isEditing', false)
      if (this.get('isEditing')) {

        return nino.save().then(()=>{

        })
      }

      let contract = this.get('model.contract');
      nino.save().then((ninoSaved) => {
        this.store.createRecord('student-contract', {
          contract: contract,
          student: nino,
          startDate: contract.contractSignatureDate
        }).save().then(() => {
          this.set('nuevoStudent', null)
          this.set('nuevo', false)
          
          //this.transitionToRoute('administrador.inscripcion.alta.alumnos', this.get('model.contract').id)
        })
      })
    },
    cancelarNino(nino) {
      
      this.set('nuevoStudent', null)
      this.set('nuevo', false)
      let student = this.get('nuevoStudent');

      if (this.get('isEditing')) {
        console.log(nino)
        this.set('isEditing', false)
        this.set('nuevoStudent', null)
        return student.rollbackAttributes();
      } else {
        this.set('isEditing', false)
        return nino.destroyRecord()
      }



    },
    agregar() {
      this.set('nuevoStudent', this.store.createRecord('student'))
      this.set('nuevo', true)
    },
    cancelar() {
      if (confirm("Seguro que quieres cancelar?")) {
        this.transitionToRoute('administrador.inicio')
      }
    },
    borrarNino(contrato) {
      contrato.get('student').then((student)=>{
      	student.destroyRecord().then(()=>{
      		contrato.destroyRecord()
      	})
      })
    },
    editarNino(contrato) {
    	contrato.get('student').then((nino)=>{
    		this.set('studentEditar', nino)
      
      this.set('isEditing', true)
    	})
      
    },
    guardarCambiosNino(){
    	let nino = this.get('studentEditar');
    	console.log(nino)
    	nino.save().then(()=>{
    		this.set('isEditing', false)
    	})
    },
    cancelarCambiosNino(){
    	let nino = this.get('studentEditar');
    	nino.rollbackAttributes();
    	this.set('isEditing', false)
    }

  }
});
