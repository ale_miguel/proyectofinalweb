import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		onGuardar(subscriber){
			subscriber.save().then((subscriberSaved)=>{
				this.transitionToRoute('administrador.inscripcion.alta.contratos', subscriberSaved.id)
			})
		},
		onCancelar(subscriber){
			if(confirm("¿Estás seguro que deseas cancelar?")){
				subscriber.destroyRecord().then(()=>{
					this.transitionToRoute('administrador.inicio')
				})
			}
			
		}
		
	}

});
