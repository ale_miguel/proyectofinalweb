import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		onGuardar(subscriber){
			subscriber.save().then(()=>{
				this.transitionToRoute('administrador.suscriptores');
			})
		},
		onCancelar(subscriber){
			if(confirm("¿Estás seguro que deseas salir sin guardar los cambios?")){
				subscriber.rollbackAttributes();
				this.transitionToRoute('administrador.suscriptores');
			}
			
		}
	}
});
