import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		editar(subscriber){
			this.transitionToRoute('administrador.suscriptores.editar', subscriber.id);
		},
		nuevoSuscriptor(){
			this.transitionToRoute('administrador.inscripcion.suscriptor')
		},
		regresar(){
			this.transitionToRoute('administrador.inicio')
		},
		filterByName(param){
			if(param != ''){
				return this.store.query('subscriber', {name:param})
			}
			else{
				return this.store.findAll('subscriber')
			}
		}
	}
});
