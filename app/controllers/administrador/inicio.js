import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		inscribir(){
			this.transitionToRoute('administrador.inscripcion.suscriptor');
		},
		agendarClase(){
			this.transitionToRoute('administrador.clases.agendar');
		},
		alumnos(){
			this.transitionToRoute('administrador.alumnos');
		},
		profesores(){
			this.transitionToRoute('administrador.profesores')
		},
		clases(){
			this.transitionToRoute('administrador.clases')
		}
	}
});
