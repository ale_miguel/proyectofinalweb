import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		perfil(teacher){
			this.transitionToRoute('administrador.profesores.perfil', teacher.id);
		},
		editar(teacher){
			this.transitionToRoute('administrador.profesores.editar', teacher.id);
		},
		nuevoProfesor(){
			this.transitionToRoute('administrador.profesores.nuevo')
		},
		regresar(){
			this.transitionToRoute('administrador.inicio')
		},
		filterByName(param){
			if(param != ''){
				console.log(param)
				return this.store.query('teacher', {name:param})
			}
			else{
				return this.store.findAll('teacher')
			}
		}
	}
});
