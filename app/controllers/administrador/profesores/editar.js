import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    guardar() {
      this.get('model').save().then(() => {
        alert("Los datos del maestro han sido actualizados");
        this.transitionToRoute('administrador.profesores.index');
      })

    },
    cancelar() {
      if (confirm("¿Estás seguro de salir sin guardar los cambios?")) {
      	this.get('model').rollbackAttributes();
        this.transitionToRoute('administrador.profesores.index')
      }
    }
  }
});
