import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		editar(){
			this.transitionToRoute('administrador.profesores.editar', this.get('model').id);
		},
		regresar(){
			this.transitionToRoute('administrador.profesores.index');
		}
	}
});
