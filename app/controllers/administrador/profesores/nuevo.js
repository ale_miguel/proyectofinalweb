import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		guardar(){
			
			this.get('model').save().then(()=>{
				alert("Se ha guarado el nuevo profesor")
				this.transitionToRoute('administrador.profesores')
			})
			
		},
		cancelar(){
			if(confirm("¿Seguro  que quieres salir sin guardar cambios?")){
				this.get('model').destroyRecord()
				this.transitionToRoute('administrador.profesores')
			}
		}
	}
});
