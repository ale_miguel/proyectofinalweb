import Controller from '@ember/controller';

export default Controller.extend({


	actions:{
		agendar(){
			this.transitionToRoute('administrador.clases.agendar')
		},
		tomarLista(lecture){
			this.transitionToRoute('administrador.clases.clase', lecture.id)
		},
		detalle(lecture){
			this.transitionToRoute('administrador.clases.clase', lecture.id)
		},
		regresar(){
			this.transitionToRoute('administrador.inicio')
		}
	}
});
