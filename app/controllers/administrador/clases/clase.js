import Controller from '@ember/controller';

export default Controller.extend({
	tomarLista:true,
	actions:{
		guardar(){

			let reservations = this.get('model.reservations')

			reservations.forEach(function (reservation) {
				reservation.save();
			})

			
			alert("Se ha guardado la asistencia y calificaciones")

			this.transitionToRoute('administrador.clases')
		},
		regresar(){
			if(this.get('tomarLista')){
				if(confirm("¿Está seguro de salir sin guardar los cambios?")){
					this.transitionToRoute('administrador.clases')
				}
			}
		}
	}
});
