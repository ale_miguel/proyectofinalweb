import Controller from '@ember/controller';
import moment from 'moment';

export default Controller.extend({

  availability: false,

  actions: {
    agendar(reservation) {
      //console.log(reservation)


      let horaS = reservation.hour.split(':')

      let am = horaS[1].split(" ");
      let hora = horaS[0] + ":" + "00 " + am[1];

      let fecha = reservation.date + " " + hora;
      console.log(fecha);

      let fechaDate = new Date(fecha);

      let fechaUnix = moment(fechaDate).unix();

      console.log(fechaUnix)

      this.store.query('lecture', { unix_date: fechaUnix }).then((lectures) => {
        if (lectures.length > 0) {
          reservation.set('lecture', lectures.firstObject)
          reservation.save().then(() => {
            alert("La clase ha sido agendada.")
            this.transitionToRoute('administrador.inicio');
          })
        } else {
          this.store.createRecord('lecture', {
            startDate: fechaDate,
            unixDate: fechaUnix,
            classroom: "Salón 1",
            teacher: reservation.teacher,
          }).save().then((lecture) => {
            reservation.set('lecture', lecture)
            reservation.save().then(() => {
              alert("La clase ha sido agendada.")
              this.transitionToRoute('administrador.inicio');
            })
          })
        }
      })



      /*alert("Clase agendada");
      this.transitionToRoute('administrador.inicio');*/
    },
    cancelar(reservation) {
      if (confirm("¿Estás seguro que deseas cancelar?")) {
        reservation.destroyRecord()
        this.transitionToRoute('administrador.inicio');
      }
    }
  }
});
