import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		inscribir(){
			this.transitionToRoute('inscripcion.suscriptor');
		},
		agendarClase(){
			this.transitionToRoute('clases.agendar');
		},
		alumnos(){
			this.transitionToRoute('alumnos');
		},
		profesores(){
			this.transitionToRoute('profesores')
		},
		clases(){
			this.transitionToRoute('clases')
		}
	}
});
