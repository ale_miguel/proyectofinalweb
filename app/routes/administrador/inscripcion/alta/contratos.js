import Route from '@ember/routing/route';

export default Route.extend({
	model(params){
		return this.store.findRecord('subscriber', params.subscriber_id).then((subscriber)=>{
			return this.store.createRecord('contract',{
				subscriber: subscriber
			})
		})
		
	}
});
