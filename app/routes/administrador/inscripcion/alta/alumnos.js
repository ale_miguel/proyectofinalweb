import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
	model(params){
		return RSVP.hash({
			contract: this.store.findRecord('contract', params.contract_id),
			studentContracts: this.store.query('student-contract', {contract_id:params.contract_id}),
			activities: this.store.findAll('activity')
		});
	}
});
