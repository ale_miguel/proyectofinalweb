import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
	model(params){
		return RSVP.hash({
			reservations: this.store.query('reservation', {lecture_id:params.lecture_id}),
			lecture: this.store.findRecord('lecture', params.lecture_id)
		}) 
		
	}
});
