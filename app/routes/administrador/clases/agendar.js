import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
	model(){
		return RSVP.hash({
			students: this.store.findAll('student'),
			teachers: this.store.findAll('teacher')
		})
	}
});
