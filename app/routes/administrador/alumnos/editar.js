import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
	model(params){
		return RSVP.hash({
			student: this.store.findRecord('student', params.student_id),
			activities: this.store.findAll('activity')
		})
		
	}
});
