import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  startDate: DS.attr('date'),
  classroom: DS.attr('string'),
  unixDate: DS.attr('number'),
  school: DS.belongsTo('school'),
  teacher: DS.belongsTo('teacher'),
  reservations: DS.hasMany('reservation'),

  activityTypeNames: computed('reservations.@each.activityName', function () {
  	return this.reservations.mapBy('activityName');
  }),

  inductionCounter: computed.sum('activityTypeNames')

});
