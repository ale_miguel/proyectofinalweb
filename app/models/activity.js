import DS from 'ember-data';
import {computed} from '@ember/object'

export default DS.Model.extend({
  lesson: DS.belongsTo('lesson'),
  activityType: DS.belongsTo('activity-type'),
  students: DS.hasMany('student'),
  reservations: DS.hasMany('reservation'),

  fullName: computed('lesson.fullName', 'activityType.name',function(){
  	return  this.lesson.get('fullName') + " " + this.activityType.get('name')
  }),

  activityTypeName: computed('activityType.name', function(){
  	return this.activityType.get('name')
  })
});
