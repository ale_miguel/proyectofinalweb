import DS from 'ember-data';
import {computed} from '@ember/object';

export default DS.Model.extend({
  currentActivity: DS.belongsTo('activity'),
  name: DS.attr('string'),
  lastName: DS.attr('string'),
  phoneNumber: DS.attr('string'),
  email: DS.attr('string'),
  testGrade: DS.attr('number'),
  reservations: DS.hasMany('reservation'),
  contracts: DS.hasMany('student-contract'),

  fullName: computed('name', 'lastName', function(){
  	return this.get('name') + " " + this.get('lastName');
  })

});
