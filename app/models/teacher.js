import DS from 'ember-data';
import {computed} from '@ember/object'
export default DS.Model.extend({
  name: DS.attr('string'),
  lastName: DS.attr('string'),
  phone: DS.attr('string'),
  mail: DS.attr('string'),
  lectures: DS.hasMany('lecture'),

  fullName: computed('name', 'lastName', function(){
  	return this.get('name') + " " + this.get('lastName');
  })
});
