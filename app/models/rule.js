import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  maxStudents: DS.attr('number'),
  school: DS.belongsTo('school'),
  activitiesRules: DS.hasMany('activity-rule'),
});
