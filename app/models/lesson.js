import DS from 'ember-data';
import {computed} from '@ember/object';

export default DS.Model.extend({
  name: DS.attr('string'),
  level: DS.belongsTo('level'),
  activities: DS.hasMany('activity'),

  fullName: computed('level.name', 'name', function(){
  	return this.level.get('name') + " " + this.name;
  })
});
