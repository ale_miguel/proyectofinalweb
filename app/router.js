import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login', {path: '/'});
  this.route('busqueda');

  this.route('administrador', function() {
    this.route('inicio');
    this.route('inscripcion', function() {
      this.route('suscriptor');
      this.route('alta', function() {
        this.route('contratos', {path: '/contratos/:subscriber_id'});
        this.route('alumnos', {path: '/alumnos/:contract_id'});
      });
    });
    this.route('alumnos', function() {
      this.route('perfil', {path: 'perfil/:student_id'});
      this.route('editar', {path: 'editar/:student_id'});
    });
    this.route('profesores', function() {
      this.route('nuevo');
      this.route('perfil', {path: 'perfil/:teacher_id'});
      this.route('editar', {path: 'editar/:teacher_id'});
    });
    this.route('clases', function() {
      this.route('agendar');
      this.route('clase', {path: '/clase/:lecture_id'});
    });
    this.route('suscriptores', function() {
      this.route('editar', {path: '/editar/:subscriber_id'});
      this.route('nuevo');
    });
  });
});

export default Router;
