import Component from '@ember/component';
import {computed} from '@ember/object';
import {inject as service} from '@ember/service';

export default Component.extend({
	store: service(),
	lecciones: computed(function(){
		return this.store.findAll('lesson')
	}),
	actions:{
		guardar(){
			this.onGuardar(this.get('student'));
		},
		cancelar(){
			this.onCancelar(this.get('student'));
		},
		setActivity(activity){
			this.get('student').set('currentActivity', activity)
			console.log(activity)
		},
		setLevel(level){
			this.set('levelSelected', level)
		}
	}
});
