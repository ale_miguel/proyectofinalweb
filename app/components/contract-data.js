import Component from '@ember/component';

export default Component.extend({
	actions:{
		guardar(){
			let contrato = this.get('contrato')

			contrato.set('contractSignatureDate', new Date(contrato.signatureDateAux))
			contrato.set('contractDueDate', new Date(contrato.dueDateAux))

			this.onGuardar();
		},
		cancelar(){
			this.onCancelar();
		}
	}
});
