import Component from '@ember/component';
import { inject as service } from '@ember/service'
import { computed } from '@ember/object';

export default Component.extend({
  reservation: null,
  store: service(),
  buttonDisabled: true,

  hasHourAndDate: computed('reservation.date', 'reservation.hour', function() {
    if (this.get('reservation.date') && this.get('reservation.hour')) {
      return true
    }

    return false;
  }),


  isAvailable: computed('hasHourAndDate', function() {
    return this.get('hasHourAndDate');
  }),

  init() {
    this._super(...arguments);
    this.set('reservation', this.get('store').createRecord('reservation'));
  },

  actions: {
    onDateClick(reservation) {

      if (!reservation.hour) {
        this.set('buttonDisabled', true);
        return null
      }

      let horaS = reservation.hour.split(':');

      let am = horaS[1].split(" ");
      let hora = horaS[0] + ":" + "00 " + am[1];

      let fecha = reservation.date + " " + hora;

      let fechaDate = new Date(fecha)

      let fechaUnix = moment(fechaDate).unix();


      this.store.query('lecture', { unix_date: fechaUnix }).then((lectures) => {
        if (lectures.length > 1) {
          this.set('buttonDisabled', true)
        } else {
          this.set('buttonDisabled', false)
        }
      })

    },
    agendar() {
      this.onAgendar(this.get('reservation'));
    },
    cancelar() {
      this.onCancelar(this.get('reservation'));
    },
    setStudent(student) {
      let reservation = this.get('reservation')
      reservation.set('student', student)
      student.get('currentActivity').then((activityStudent) => {
        reservation.set('activity', activityStudent)
        this.set('studentSelected', student)
      })

    },
    agregarNino() {
      this.set('studentIsSelected', true)
    },
    modificarNino() {
      this.set('studentIsSelected', false)
    },
    setTeacher(teacher) {
      let reservation = this.get('reservation')
      reservation.set('teacher', teacher)
      this.set('teacherSelected', teacher)
    },
    agregarProfesor() {
      this.set('teacherIsSelected', true)
    },
    modificarProfesor() {
      this.set('teacherIsSelected', false)
    }
  }
});
