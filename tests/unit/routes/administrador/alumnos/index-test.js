import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | administrador/alumnos/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:administrador/alumnos/index');
    assert.ok(route);
  });
});
