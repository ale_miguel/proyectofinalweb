import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | administrador/inicio', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:administrador/inicio');
    assert.ok(route);
  });
});
