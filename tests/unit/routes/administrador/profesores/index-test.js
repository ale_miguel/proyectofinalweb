import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | administrador/profesores/index', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:administrador/profesores/index');
    assert.ok(route);
  });
});
